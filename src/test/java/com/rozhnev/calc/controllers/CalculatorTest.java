package com.rozhnev.calc.controllers;
import com.rozhnev.calc.service.Calculator;
import com.rozhnev.calc.service.CalculatorSoap;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.*;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorTest {

    private Calculator calculator;
    private CalculatorSoap soap;

    @Before
    public void before(){
        calculator = new Calculator();
        soap = calculator.getCalculatorSoap();
    }

    @Test
    public void Add_calc(){
        int result = soap.add(1,2);
        System.out.println(result);
        assertNotNull(result);
    }

    @Test
    public void Subtract_calc(){
        int result = soap.subtract(1,2);
        System.out.println(result);
        assertNotNull(result);
    }

    @Test
    public void Divide_calc(){
        int result = soap.divide(4,2);
        System.out.println(result);
        assertNotNull(result);
    }

    @Test
    public void Multiply_calc(){
        int result = soap.multiply(11298,2);
        System.out.println(result);
        assertNotNull(result);
    }

}
