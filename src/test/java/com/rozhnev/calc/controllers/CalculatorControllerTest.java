package com.rozhnev.calc.controllers;

import com.rozhnev.calc.service.CalculatorService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CalculatorControllerTest {

    @Autowired
    private CalculatorService calculatorService;

    private MockMvc mockMvc;

    @Before
    public void before() {
        mockMvc = MockMvcBuilders
                .standaloneSetup(new CalculatorController(calculatorService))
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();
    }

    @Test
    public void test(){
        int i;
        for(i =0; i< 102; i++) {
            try {
                mockMvc.perform( get("/addition/1,2") )
                        .andExpect(status().isOk());
            } catch (Exception ex){
                assertTrue(i == 100);
            }
        }
    }

}