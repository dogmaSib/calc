package com.rozhnev.calc.controllers;

import com.rozhnev.calc.service.CalculatorService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class CalculatorController {
    @Autowired
    private final CalculatorService calculatorService;

    @GetMapping("/addition/{values}")
    public Integer addition(@PathVariable Integer[] values) {
        return calculatorService.add(values);
    }
    @GetMapping("/subtraction/{values}")
    public Integer subtraction(@PathVariable Integer[] values) {
        return calculatorService.subtract(values);
    }
    @GetMapping("/division/{values}")
    public Integer division(@PathVariable Integer[] values) {
        return calculatorService.divide(values);
    }
    @GetMapping("/multiplication/{values}")
    public Integer multiplication(@PathVariable Integer[] values) {
        return calculatorService.multiply(values);
    }

    @GetMapping("/operation/{values}")
    public Integer operation(@PathVariable Integer[] values) {
        return calculatorService.operation(values);
    }
}
