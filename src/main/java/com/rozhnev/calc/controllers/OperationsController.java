package com.rozhnev.calc.controllers;

import com.rozhnev.calc.dto.OperationBean;
import com.rozhnev.calc.service.OperationService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class OperationsController {

    @Autowired
    private OperationService operationService;

    @GetMapping("/get/{id}")
    public OperationBean addition(@PathVariable Long id) {
        return operationService.getOperation(id);
    }

}
