package com.rozhnev.calc.persistence;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;
@Entity
@Table(name = "operation")
@Getter
@Setter
public class Operation {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @Column(name = "operation_type")
    @Enumerated(EnumType.STRING)
    private OperationType operationType;

    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

    @Column(name = "intA")
    private Integer intA;

    @Column(name = "intB")
    private Integer intB;

    @Column(name = "intC")
    private Integer intC;

    @Column(name = "result")
    private Integer result;
}
