package com.rozhnev.calc.persistence;

public enum OperationType {
    ADD, SUBTRACT, DIVIDE, MULTIPLY, OTHER
}
