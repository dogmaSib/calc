package com.rozhnev.calc.dto;


import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "multiplyResult"
})
@XmlRootElement(name = "MultiplyResponse")
@Getter
@Setter
public class MultiplyResponse {

    @XmlElement(name = "MultiplyResult")
    protected int multiplyResult;

}
