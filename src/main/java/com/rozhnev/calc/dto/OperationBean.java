package com.rozhnev.calc.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.rozhnev.calc.persistence.OperationType;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class OperationBean {
    private OperationType operationType;
    private Date date;
    private Integer intA;
    private Integer intB;
    private Integer intC;
    private Integer result;
}
