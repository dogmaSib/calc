package com.rozhnev.calc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "addResult"
})
@XmlRootElement(name = "AddResponse")
@Getter
@Setter
public class AddResponse {

    @XmlElement(name = "AddResult")
    protected int addResult;

}
