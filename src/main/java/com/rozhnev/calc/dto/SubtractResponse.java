package com.rozhnev.calc.dto;


import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "subtractResult"
})
@XmlRootElement(name = "SubtractResponse")
@Getter
@Setter
public class SubtractResponse {

    @XmlElement(name = "SubtractResult")
    protected int subtractResult;

}
