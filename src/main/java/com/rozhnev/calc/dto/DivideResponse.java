package com.rozhnev.calc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "divideResult"
})
@XmlRootElement(name = "DivideResponse")
@Getter
@Setter
public class DivideResponse {

    @XmlElement(name = "DivideResult")
    protected int divideResult;

}
