package com.rozhnev.calc.dto;

import lombok.Getter;
import lombok.Setter;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
        "intA",
        "intB"
})
@XmlRootElement(name = "Add")
@Getter
@Setter
public class Add {
    private int intA;
    private int intB;
}
