package com.rozhnev.calc.service;


import org.springframework.stereotype.Component;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import java.net.MalformedURLException;
import java.net.URL;

@WebServiceClient(name = "Calculator",
        wsdlLocation = "http://www.dneonline.com/calculator.asmx?WSDL",
        targetNamespace = "http://tempuri.org/")
@Component
public class Calculator extends Service {

    public final static URL WSDL_LOCATION;

    public final static QName SERVICE = new QName("http://tempuri.org/", "Calculator");
    public final static QName CalculatorSoap = new QName("http://tempuri.org/", "CalculatorSoap");
    static {
        URL url = null;
        try {
            url = new URL("http://www.dneonline.com/calculator.asmx?WSDL");
        } catch (MalformedURLException e) {
            java.util.logging.Logger.getLogger(Calculator.class.getName())
                    .log(java.util.logging.Level.INFO,
                            "Can not initialize the default wsdl from {0}", "http://www.dneonline.com/calculator.asmx?WSDL");
        }
        WSDL_LOCATION = url;
    }

    public Calculator() {
        super(WSDL_LOCATION, SERVICE);
    }

    @WebEndpoint(name = "CalculatorSoap")
    public CalculatorSoap getCalculatorSoap() {
        return super.getPort(CalculatorSoap, CalculatorSoap.class);
    }

}
