package com.rozhnev.calc.service;

import com.rozhnev.calc.dto.OperationBean;
import com.rozhnev.calc.persistence.OperationType;
import com.rozhnev.calc.persistence.Operation;
import com.rozhnev.calc.repository.OperationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.util.Date;

@Service
@EnableScheduling
@RequiredArgsConstructor
public class OperationService {

    @Autowired
    private OperationRepository operationRepository;

    void saveOperation(Integer[] values, OperationType operationType, Integer result) {
        Operation operation = new Operation();
        operation.setDate(new Date());
        operation.setIntA(values[0]);
        operation.setIntB(values[1]);
        if(values.length == 3) {
            operation.setIntA(values[2]);
        } else {
            operation.setIntA(null);
        }
        operation.setOperationType(operationType);
        operation.setResult(result);
        operationRepository.save(operation);
    }

    public OperationBean getOperation(Long id) {
        Operation operation = operationRepository.getOne(id);
        OperationBean bean = new OperationBean();
        BeanUtils.copyProperties(operation, bean);
        return bean;
    }

    @Scheduled(fixedDelayString = "${interval.cache}")
    public void updateHistory() {

    }

}
