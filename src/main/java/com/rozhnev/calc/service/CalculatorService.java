package com.rozhnev.calc.service;

import com.rozhnev.calc.persistence.OperationType;
import lombok.RequiredArgsConstructor;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicInteger;

@Service
@RequiredArgsConstructor
public class CalculatorService {

    @Value("${operations.max}")
    private Integer maxOperations;

    private AtomicInteger operationCount;

    private Integer currentHour;

    @Autowired
    private Calculator calculator;

    @Autowired
    private OperationService operationService;

    @PostConstruct
    void init(){
        currentHour = getHourOfDay();
        operationCount = new AtomicInteger(0);
    }

    public int add(Integer[] values){
        if(values.length != 2){
            throw new RuntimeException("Must be 2 values");
        }
        checkOperationsByLastHour();
        Integer result = calculator.getCalculatorSoap().add(values[0], values[1]);
        operationService.saveOperation(values, OperationType.ADD, result);
        return result;
    }

    public int subtract(Integer[] values){
        if(values.length != 2){
            throw new RuntimeException("Must be 2 values");
        }
        checkOperationsByLastHour();
        Integer result = calculator.getCalculatorSoap().subtract(values[0], values[1]);
        operationService.saveOperation(values, OperationType.SUBTRACT, result);
        return result;
    }

    public int divide(Integer[] values){
        if(values.length != 2){
            throw new RuntimeException("Must be 2 values");
        }
        checkOperationsByLastHour();
        Integer result = calculator.getCalculatorSoap().divide(values[0], values[1]);
        operationService.saveOperation(values, OperationType.DIVIDE, result);
        return result;
    }

    public int multiply(Integer[] values){
        if(values.length != 2){
            throw new RuntimeException("Must be 2 values");
        }
        checkOperationsByLastHour();
        Integer result = calculator.getCalculatorSoap().multiply(values[0], values[1]);
        operationService.saveOperation(values, OperationType.MULTIPLY, result);
        return result;
    }

    /**
     * must be a+b/c
     * example 10+10/5 = 12
     */
    public int operation(Integer[] values){
        if(values.length != 3){
            throw new RuntimeException("Must be 3 values");
        }
        checkOperationsByLastHour();
        int intB = calculator.getCalculatorSoap().divide(values[1], values[2]);
        Integer result = calculator.getCalculatorSoap().add(values[0], intB);

        operationService.saveOperation(values, OperationType.OTHER, result);

        return result;
    }

    private void checkOperationsByLastHour(){
        if(maxOperations.compareTo(operationCount.getAndIncrement()) == 0) {
            throw new RuntimeException("Reached the maximum number of operations.");
        }

        if(currentHour.compareTo(getHourOfDay()) < 0) {
            currentHour = getHourOfDay();
            operationCount.set(1);
        }

    }

    public int getHourOfDay() {
        return  new DateTime().getHourOfDay();
    }
}
