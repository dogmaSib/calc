package com.rozhnev.calc;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CalcService {
    public static void main(String[] args) {
        SpringApplication.run(CalcService.class, args);
    }
}
